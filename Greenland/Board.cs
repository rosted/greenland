﻿using System;

namespace Greenland
{
    public class Board
    {
        private int Size;
        private Position currentPosition = new Position { X = 0, Y = 0 };
        private Piece[,] pieces;

        public Board(int size)
        {
            Size = size;
            pieces = new Piece[Size, Size];
        }

        public bool AddPiece(Piece piece)
        {
            if (CanAdd(piece))
            {
                pieces[currentPosition.X, currentPosition.Y] = piece;

                currentPosition = GetNextPosition();

                return true;
            }

            return false;
        }

        public void RemovePiece()
        {
            var previousPosition = GetPreviousPosition();
            pieces[previousPosition.X, previousPosition.Y] = null;
            currentPosition = previousPosition;
        }

        private bool CanAdd(Piece piece)
        {
            var mismatchVertical = (currentPosition.Y > 0 && ((piece.Up.Letter != pieces[currentPosition.X, currentPosition.Y - 1].Down.Letter) || piece.Up.BodyPart == pieces[currentPosition.X, currentPosition.Y - 1].Down.BodyPart));
            var mismatchHorizontal = (currentPosition.X > 0 && ((piece.Left.Letter != pieces[currentPosition.X - 1, currentPosition.Y].Right.Letter) || piece.Left.BodyPart == pieces[currentPosition.X - 1, currentPosition.Y].Right.BodyPart));

            return !mismatchVertical && !mismatchHorizontal;
        }

        public void Print()
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    var current = pieces[x, y];

                    if (current == null)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.SetCursorPosition(x * 8, y * 4);
                        Console.Write("      ");
                        Console.SetCursorPosition(x * 8, y * 4 + 1);
                        Console.Write("      ");
                        Console.SetCursorPosition(x * 8, y * 4 + 2);
                        Console.Write("      ");
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;

                        Console.SetCursorPosition(x * 8, y * 4);
                        Console.Write($"  {current.Up}  ");
                        Console.SetCursorPosition(x * 8, y * 4 + 1);
                        Console.Write($"{current.Left}");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write($" {current.Id}");
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write($"{ current.Right}");
                        Console.SetCursorPosition(x * 8, y * 4 + 2);
                        Console.Write($"  {current.Down}  ");
                    }
                }
            }
            Console.WriteLine();
            Console.ResetColor();
        }

        private Position GetNextPosition()
        {
            var nextPosition = currentPosition;

            if (nextPosition.X < Size - 1)
            {
                nextPosition.X++;
            }
            else
            {
                nextPosition.X = 0;

                if (nextPosition.Y < Size - 1)
                {
                    nextPosition.Y++;
                }
            }

            return nextPosition;
        }

        private Position GetPreviousPosition()
        {
            var previousPosition = currentPosition;

            if (previousPosition.X > 0)
            {
                previousPosition.X--;
            }
            else
            {
                previousPosition.Y--;
                previousPosition.X = Size - 1;
            }

            return previousPosition;
        }

        public struct Position
        {
            public int X { get; set; }

            public int Y { get; set; }
        }
    }
}
