﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Greenland
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            var availablePieces = JsonConvert.DeserializeObject<List<Piece>>(File.ReadAllText("greenland.json"));
            var board = new Board((int)Math.Sqrt(availablePieces.Count));

            var placementEngine = new PlacementEngine(board);
            var result = placementEngine.PlacePieces(availablePieces);

            if (result)
            {
                board.Print();
                Console.WriteLine($"Success");
            }
            else
            {
                Console.WriteLine($"Not possible");
            }

            Console.ReadKey();
        }
    }
}