﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Greenland
{
    public class Piece
    {
        public Piece(int id, Part up, Part right, Part down, Part left)
        {
            Id = id;
            Up = up;
            Right = right;
            Down = down;
            Left = left;
        }

        public int Id { get; set; }

        public Part Up { get; set; }

        public Part Right { get; set; }

        public Part Down { get; set; }

        public Part Left { get; set; }

        public void Rotate()
        {
            var up = Up;
            Up = Right;
            Right = Down;
            Down = Left;
            Left = up;
        }

        public class Part
        {
            public char Letter { get; set; }

            [JsonConverter(typeof(StringEnumConverter))]
            public BodyPart BodyPart { get; set; }

            public Part(char letter, BodyPart bodyPart)
            {
                Letter = letter;
                BodyPart = bodyPart;
            }

            public override string ToString()
            {
                var bodyPartLetter = BodyPart == BodyPart.Upper ? '↑' : '↓';

                return $"{Letter}{bodyPartLetter}";
            }
        }

        public enum BodyPart
        {
            Upper,
            Lower
        }
    }
}
