﻿using System.Collections.Generic;
using System.Linq;

namespace Greenland
{
    public class PlacementEngine
    {
        private Board board;

        public PlacementEngine(Board board)
        {
            this.board = board;
        }

        public bool PlacePieces(List<Piece> availablePieces)
        {
            return PlacePiece(availablePieces);
        }

        public bool PlacePiece(List<Piece> availablePieces)
        {
            foreach (var currentPiece in availablePieces)
            {
                var added = TryAddPiece(currentPiece);

                if (added)
                {
                    var reducedList = RemovePieceFromList(availablePieces, currentPiece);

                    if (!reducedList.Any())
                    {
                        return true;
                    }

                    var result = PlacePiece(reducedList);

                    if (!result)
                    {
                        board.RemovePiece();
                    }
                    else
                    {
                        return result;
                    }
                }
            }

            return false;
        }

        private List<Piece> RemovePieceFromList(List<Piece> availablePieces, Piece currentPiece)
        {
            var reducedList = new List<Piece>(availablePieces);
            reducedList.Remove(currentPiece);
            return reducedList;
        }

        private bool TryAddPiece(Piece currentPiece)
        {
            bool added;
            var rotations = 0;

            do
            {
                added = board.AddPiece(currentPiece);
                if (!added)
                {
                    currentPiece.Rotate();
                    rotations++;
                }
            }
            while (!added && rotations < 4);

            return added;
        }
    }
}
