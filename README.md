Solver for the "Greenland Drillespil" written in C#

[![Build status](https://ci.appveyor.com/api/projects/status/t4b400r9mtq8k4m9/branch/master?svg=true)](https://ci.appveyor.com/project/rosted/greenland/branch/master)

![Capture.PNG](https://bitbucket.org/repo/A58jpp/images/2883877590-Capture.PNG)

![Solution.jpg](https://bitbucket.org/repo/A58jpp/images/793135644-Solution.jpg)